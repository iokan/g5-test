const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/styles/base/_variables.scss";
          @import "@/styles/base/_mixins.scss";
        `,
        implementation: require('sass'),
      }
    }
  }
})
